import random
import json
import numpy as np
import csv
def access_bat_bowl(batsman,bowler):
    with open("final_batsmen_cluster.csv","r") as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
        for row in csvreader:
            if(row[4]==batsman):
                x = row[3]
                break
        batsmen_list=[]
        for row in csvreader:
            if(row[3]==x):
                batsmen_list.append(row[4])

    with open ("final_bowler_cluster.csv","r") as csvfile:
        csvreader = csv.reader(csvfile, delimiter =",",quoting=csv.QUOTE_MINIMAL)
        for row in csvreader:
            if(row[7]==bowler):
                y = row[6]
                break
        bowler_list=[]
        for row in csvreader:
            if(row[6]==y):
                bowler_list.append(row[7])
    
    return batsmen_list,bowler_list
newdict=json.load(open("dict.txt"))
def makePred(batsman,bowler,limit):
	if float(newdict.get(bowler,0)!=0 and newdict.get(bowler,0).get(batsman,0)!=0 and newdict.get(bowler,0).get(batsman,0).get('balls',0))>=limit:
		balls=int(newdict.get(bowler,0).get(batsman,0).get('balls',0))
		dots=float(newdict.get(bowler,0).get(batsman,0).get('0',0))/balls
		ones=float(newdict.get(bowler,0).get(batsman,0).get('1',0))/balls
		twos=float(newdict.get(bowler,0).get(batsman,0).get('2',0))/balls
		threes=float(newdict.get(bowler,0).get(batsman,0).get('3',0))/balls
		fours=float(newdict.get(bowler,0).get(batsman,0).get('4',0))/balls
		fives=float(newdict.get(bowler,0).get(batsman,0).get('5',0))/balls
		sixes=float(newdict.get(bowler,0).get(batsman,0).get('6',0))/balls
		wickets=float(newdict.get(bowler,0).get(batsman,0).get('w',0))/balls
		c=np.random.choice([0,1,2,3,4,5,6],1,[dots,ones,twos,threes,fours,fives,sixes])
		#print(c[0])
		#print(dots,ones,twos,threes,fours,fives,sixes,"\n")
		if(len(c)!=1):
			print("9231111111118888888676973264863124987641329")
			return -1
			
		else:
			return int(c[0])
	else:
			#print("Needs clustering")
			return -1
def makePredClus(batsman,bowler):
	limit=10
	bats , bows = access_bat_bowl(batsman,bowler)
	print("clusters with ",len(bats),len(bows),"\n")
	if(int(makePred(batsman,bowler,limit))!=-1):
		#print("using data for",batsman,bowler,"\n")
		return makePred(batsman,bowler,limit)
	else:
		s=0
		count=0
		if(len(bats)!=0 and len(bows)!=0):
			for bat in bats:
				for bow in bows:
					if makePred(bat,bow,1)!=-1:
						#print("using data for",bat,bow,"\n")
						count=count+1
						s=s+int(makePred(bat,bow,1))
			if count!=0:
				return s/count
		
		print("AVG")
		return int(makePred("average","average",0))%4 #fixing max avg to lie between [0..3]		
	#return 1
def updateBatsman(batsman,bowler,prob,limit):
	if float(newdict.get(bowler,0)!=0 and newdict.get(bowler,0).get(batsman,0)!=0 and newdict.get(bowler,0).get(batsman,0).get('balls',0))>=limit:
		#print("Notout: ",float(prob)-float(float(newdict.get(bowler,0).get(batsman,0).get('w',0))/float(newdict.get(bowler,0).get(batsman,0).get('balls',0))),"\n")
		return float(prob)-float(float(newdict.get(bowler,0).get(batsman,0).get('w',0))/float(newdict.get(bowler,0).get(batsman,0).get('balls',0)))
	else:
		return -1
def updateBatsmanClus(batsman,bowler,prob):
	limit=10
	bats , bows = access_bat_bowl(batsman,bowler)
	#print("clusters with ",len(bats),len(bows),"\n")
	if(int(updateBatsman(batsman,bowler,prob,limit))!=-1):
		#print("using data for",batsman,bowler,"\n")
		return updateBatsman(batsman,bowler,prob,limit)
	else:
		s=0
		count=0
		if(len(bats)!=0 and len(bows)!=0):
			for bat in bats:
				for bow in bows:
					if updateBatsman(bat,bow,prob,1)!=-1:
						#print("using data for",bat,bow,"\n")
						count=count+1
						s=s+float(updateBatsman(bat,bow,prob,1))
			if(count!=0):
				return s/count
		
		print("AVG")
		return float(updateBatsman("average","average",prob,0)) #fixing max avg to lie between [0..3]	
score1=0	
score2=0
inn=1
overs=0
maxovers=20
team1batsmen=["PP Shaw","C Munro","SS Iyer","RR Pant","GJ Maxwell","V Shankar","LE Plunkett","A Mishra","S Nadeem","Avesh Khan","TA Boult"]
team1bowlers=["S Nadeem","TA Boult","Avesh Khan","LE Plunkett","A Mishra"]
#196-6
team2batsmen=["DJM Short","JC Butler","SV Samson","BA Stokes","RA Tripathi","K Gowtham","JC Archer","AM Rahane","JD Unadkat","DS Kulkarni","S Gopal"]
team2bowlers=["DS Kulkarni","JC Archer","K Gowtham","JD Unadkat","BA Stokes"]
#146-5
strike=team1batsmen[:2]
wickets=0
notout=[1,1]
while(overs!=20 and wickets<10):
	for balls in range(0,6):
		notout[0]=updateBatsmanClus(strike[0],team2bowlers[(overs)%5],notout[0])
		if notout[0]<0.5:#batsman needs to be dismissed
			wickets=wickets+1
			if(wickets!=10):
				strike[0]=team1batsmen[wickets+1]
			notout[0]=1
			run="W"
		else:
			run=int(makePredClus(strike[0],team2bowlers[(overs)%5]))
			if(run%2==1):#need to switch striker and non striker
				strike.reverse()
				notout.reverse()
			score1=score1+run
		print(score1,"-",wickets,"(",overs,".",balls,")\t+",run,"|",strike[0],"|",strike[1],"\t|",team1bowlers[(overs)%5],"\n")
		if(wickets==10):
			break
	strike.reverse()#switch strikers at end of over
	notout.reverse()
	overs=overs+1
print("Total T1 score:",score1)

strike=team2batsmen[:2]
notout=[1,1]
overs=0
wickets=0
while(overs!=20 and wickets<10 and score1>=score2):
	for balls in range(0,6):
		notout[0]=updateBatsmanClus(strike[0],team1bowlers[(overs)%5],notout[0])
		if(score2>score1):
			break
		if notout[0]<0.5:#batsman needs to be dismissed
			wickets=wickets+1
			if(wickets!=10):
				strike[0]=team2batsmen[wickets+1]
			notout[0]=1
			run="W"
		else:
			run=int(makePredClus(strike[0],team1bowlers[(overs)%5]))
			if(run%2==1):#need to switch striker and non striker
				strike.reverse()
				notout.reverse()
			score2=score2+run
		print(score2,"-",wickets,"(",overs,".",balls,")\t+",run,"|",strike[0],"\t",strike[1],"\t|",team1bowlers[(overs)%5],"\n")
		if(wickets==10 or score2>score1):
			break
	strike.reverse()#switch strikers at end of over
	notout.reverse()
	overs=overs+1
if score2>score1:
	print("score of team1 is ",score1)
	print("score of team2 is ",score2)
	print("Team 2 won with ",20-overs,"overs to spare!\n")
else:
	print("score of team1 is ",score1)
	print("score of team2 is ",score2)
	print("Team 1 won with ",score1-score2,"runs\n")
