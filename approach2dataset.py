import csv 
import sys
import json
import os
import collections
import time
import random
import numpy as np
starttime=time.time()
directory="C:/Nandos/Engineering/3rd year/Sem 5/BIGDATA/BIGDATA/A1/ipl_csv/"
#bowlerdict= collections.defaultdict(dict)
dismissals={'caught','bowled','stumped','caught and bowled','lbw','hit wicket'}

def access_bat_bowl(batsman1,batsman2,bowler):
	x=16
	y=16
	z=16
	flag=0
	with open("final_batsmen_cluster.csv","r") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
		for row in csvreader:
			if(row[4]==batsman1):
				x = row[3]
				flag=flag+1
			elif(row[4]==batsman2):
				y= row[3]
				flag=flag+1
			if (flag==2):
				break
	with open ("final_bowler_cluster.csv","r") as csvfile:
		csvreader = csv.reader(csvfile, delimiter =",",quoting=csv.QUOTE_MINIMAL)
		for row in csvreader:
			if(row[7]==bowler):
				z = row[6]
				break
	return x,y,z
def access_bat_stats(batsman):
	avg=0
	sr=0
	flag=0
	with open("final_batsmen_cluster.csv","r") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
		for row in csvreader:
			if(row[4]==batsman):
				sr = row[2]
				avg = row[1]
				break
	return sr,avg

def access_bow_stats(bowler):
	eco=0
	sr=0
	avg=0
	wickets=0
	flag=0
	#id,overs,wickets,avg,eco,sr
	with open("final_bowler_cluster.csv","r") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
		for row in csvreader:
			if(row[7]==bowler):
				sr = row[5]
				eco= row[4]
				avg = row[3]
				wickets=row[2]
				break
	return sr,avg,eco,wickets

def nested_dict(n, type):
    if n == 1:
        return collections.defaultdict(type)
    else:
        return collections.defaultdict(lambda: nested_dict(n-1, type))
bowlerdict = nested_dict(2, int)
op1=open("try.csv",'w')
op1.close()
op2=open("tryw.csv",'w')
op2.close()
op=open("try.csv",'a')
op3=open("tryw.csv",'a')
for path in os.listdir(directory):
	#print(directory+path)
	fi=open(directory+path, 'r')
	reader = csv.reader(fi)
	o='-1'
	for row in reader:
		if row[0]=='ball':
			if(o!=str(row[2]).split('.')[0]):
				if(o!='-1'):
					sr,avg,eco,wickets=access_bow_stats(bn)
					srb1,avgb1=access_bat_stats(b1n)
					srb2,avgb2=access_bat_stats(b2n)
					#label,bat1clus,bat2clus,bowclus,bat1sr,bat1avg,bat2sr,bat2avg,bowsr,bowavg,bowwickets
					lines=str(score)+","+str(b1)+","+str(b2)+","+str(b)+","+str(srb1)+","+str(avgb1)+","+str(srb2)+","+str(avgb2)+","+str(sr)+","+str(avg)+","+str(wickets)+"\n"
					linew=str(wicket)+","+str(b1)+","+str(b2)+","+str(b)+","+str(srb1)+","+str(avgb1)+","+str(srb2)+","+str(avgb2)+","+str(sr)+","+str(avg)+","+str(wickets)+"\n"
					op.write(lines)
					op3.write(linew)
				score=0
				wicket=0
				balls=1
				b1n=str(row[5])#non striker
				b2n=str(row[4])#batsman
				bn=str(row[6])#bowler
				b1,b2,b=access_bat_bowl(b1n,b2n,bn)
				o=str(row[2]).split('.')[0]
			else:
				if (row[9]):
					wicket=wicket+1
				else:
					score=score+int(str(row[7]))
				balls=balls+1
	fi.close()
op.close()
op3.close()
#print(bowlerdict['average']['average']['6'])
endtime=time.time()
print("Time:",endtime-starttime)
# newdict=json.load(open("dict.txt"))
# try:
# except KeyError:
# 	print("Error")