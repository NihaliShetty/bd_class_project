import csv 
import sys
import json
import os
import collections
import time
starttime=time.time()
directory="C:/Nandos/Engineering/3rd year/Sem 5/BIGDATA/BIGDATA/A1/ipl_csv/"
#bowlerdict= collections.defaultdict(dict)
dismissals={'caught','bowled','stumped','caught and bowled','lbw','hit wicket'}
def nested_dict(n, type):
    if n == 1:
        return collections.defaultdict(type)
    else:
        return collections.defaultdict(lambda: nested_dict(n-1, type))
bowlerdict = nested_dict(3, int)
for path in os.listdir(directory):
	#print(directory+path)
	fi=open(directory+path, 'r')
	reader = csv.reader(fi)
	for row in reader:
		if row[0]=='ball':
			if (row[9]):
				e='w'#wickets
			else:
				e=str(row[7])#runs
			c=str(row[6])#bowler who conceeded runs
			d=str(row[4])#batsman who scored
			#cd=
			f='balls'
			if (bowlerdict.get(c)):
				if (bowlerdict.get(c).get(d)):
					if (bowlerdict.get(c).get(d).get(e)):
						bowlerdict[c][d][e]=int(bowlerdict[c][d][e])+1
					else:
						bowlerdict[c][d][e]=1
					if (bowlerdict.get(c).get(d).get(f)):
						bowlerdict[c][d][f]=int(bowlerdict[c][d][f])+1
					else:		
						bowlerdict[c][d][f]=1
				else:
						bowlerdict[c][d][f]=1
			else:
				bowlerdict[c][d][f]=1
	fi.close()
#print(bowlerdict['RA Jadeja']['SR Watson']['6'])
json.dump(bowlerdict,open("dict.txt",'w'))
endtime=time.time()
# print("Time:",endtime-starttime)
# newdict=json.load(open("dict.txt"))
# try:
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('0',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('1',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('2',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('3',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('4',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('5',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('6',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('7',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('8',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('9',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('10',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('w',0))
# 	print(newdict.get('RA Jadeja',0).get('SR Watson',0).get('balls',0))
# except KeyError:
# 	print("Error")