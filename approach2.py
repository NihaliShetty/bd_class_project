from pyspark.ml import Pipeline
from pyspark.ml.regression import DecisionTreeRegressor
from pyspark.ml.feature import VectorIndexer
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.sql import SparkSession
from pyspark.ml.feature import OneHotEncoderEstimator, StringIndexer, VectorAssembler
from pyspark.mllib.regression import LabeledPoint

from pyspark.mllib.tree import DecisionTree, DecisionTreeModel
from pyspark.mllib.util import MLUtils
from pyspark.context import SparkContext
from numpy import array
import math
import random
import json
import csv 
import sys
import json
import os
import collections
import time
import random
import numpy as np
starttime=time.time()

import csv

sc = SparkContext("local","test")

#returns LabeledPoint dataframe to be used as input to decision tree for prediction of runs
def runs_function(val):
    cluster_b_1 = int(val[1])
    cluster_b_2 = int(val[2])
    cluster_bow = int(val[3])
    sr1 = float(val[4])
    avg1 = float(val[5])
    sr2 = float(val[6])
    avg2 = float(val[7])
    bow_sr = float(val[8])
    bow_avg = float(val[9])
    # bow_eco = float(val[9])
    bow_wkts = float(val[10])
    output = int(val[0])
    return LabeledPoint(output , array([cluster_b_1,cluster_b_2, cluster_bow, sr1 , avg1, sr2, avg2, bow_sr, bow_avg,bow_wkts]))

#returns LabeledPoint dataframe to be used as input to decision tree for prediction of wickets
def wkts_function(val):
    cluster_b_1 = int(val[1])
    cluster_b_2 = int(val[2])
    cluster_bow = int(val[3])
    sr1 = float(val[4])
    avg1 = float(val[5])
    sr2 = float(val[6])
    avg2 = float(val[7])
    bow_sr = float(val[8])
    bow_avg = float(val[9])
    # bow_eco = float(val[9])
    bow_wkts = float(val[10])
    output = float(val[0])
    return LabeledPoint(output , array([cluster_b_1,cluster_b_2, cluster_bow, sr1 , avg1, sr2, avg2, bow_sr, bow_avg,bow_wkts]))



data = sc.textFile("try.csv")
map_data = data.map(lambda x : x.split(','))
final_data = map_data.map(runs_function)

data2 = sc.textFile("tryw.csv")
map_data2 = data2.map(lambda x : x.split(','))
final_data2 = map_data2.map(wkts_function)



# model1 = DecisionTree.trainRegressor(sc.parallelize(data), 36, categoricalFeaturesInfo={0:17})
model1 = DecisionTree.trainRegressor(final_data, categoricalFeaturesInfo={}, maxDepth=20)   # training the two models
model2 = DecisionTree.trainRegressor(final_data2, categoricalFeaturesInfo={}, maxDepth=15)

print(model1)
print(model1.toDebugString())


dismissals={'caught','bowled','stumped','caught and bowled','lbw','hit wicket'}


#function for accessing cluster number of for the two batsmen and the bowler
def access_bat_bowl(batsman1,batsman2,bowler):
	x=-1
	y=-1
	z=-1
	flag=0
	with open("final_batsmen_cluster.csv","r") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
		for row in csvreader:
			if(row[4]==batsman1):
				x = row[3]
				flag=flag+1
			elif(row[4]==batsman2):
				y= row[3]
				flag=flag+1
			if (flag==2):
				break
	with open ("final_bowler_cluster.csv","r") as csvfile:
		csvreader = csv.reader(csvfile, delimiter =",",quoting=csv.QUOTE_MINIMAL)
		for row in csvreader:
			if(row[7]==bowler):
				z = row[6]
				break
	return x,y,z


#returns the batsman statistics that is strike rate and average
def access_bat_stats(batsman):
	avg=-1
	sr=-1
	flag=0
	with open("final_batsmen_cluster.csv","r") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
		for row in csvreader:
			if(row[4]==batsman):
				sr = row[2]
				avg = row[1]
				break
	return sr,avg


# returns the bowler statistics, that is strikerate , average and wickets
def access_bow_stats(bowler):
	eco=-1
	sr=-1
	avg=-1
	wickets=-1
	flag=0
	#id,overs,wickets,avg,eco,sr
	with open("final_bowler_cluster.csv","r") as csvfile:
		csvreader = csv.reader(csvfile, delimiter=',',quoting=csv.QUOTE_MINIMAL)
		for row in csvreader:
			if(row[7]==bowler):
				sr = row[5]
				eco= row[4]
				avg = row[3]
				wickets=row[2]
				break
	return sr,avg,wickets



#initializing 
score1=0	
score2=0
inn=1
overs=0
maxovers=20

# team1batsmen=["Q de Kock","SS Iyer","SV Samson","KK Nair","SW Billings","CH Morris","CR Braithwaite","RR Pant","Mohammed Shami","A Mishra","Z Khan"] 
# team1bowlers=["Z Khan","Mohammed Shami","CH Morris","CR Braithwaite","A Mishra"]
# #gl 172/6
# team2batsmen=["Q de Kock","SS Iyer","SV Samson","KK Nair","SW Billings","CH Morris","CR Braithwaite","RR Pant","Mohammed Shami","A Mishra","Z Khan"] 
# team2bowlers=["Z Khan","Mohammed Shami","CH Morris","CR Braithwaite","A Mishra"]
# #d 171/5

# team1batsmen=["CH Gayle","V Kohli","AB de Villiers","SR Watson","SN Khan","KM Jadhav","STR Binny","Parvez Rasool","HV Patel","AF Milne","YS Chahal"] 
# team1bowlers=["AF Milne","SR Watson","Parvez Rasool","HV Patel","YS Chahal"]
# # rcb 227/4 2016 4th match
# team2batsmen=["DA Warner","S Dhawan","MC Henriques","NV Ojha","DJ Hooda","EJG Morgan","A Ashish Reddy","KV Sharma","A Nehra","B Kumar","Mustafizur Rahman"]
# team2bowlers=["A Nehra","B Kumar","Mustafizur Rahman","MC Henriques","KV Sharma"]
# # srh 182/6

# team1batsmen=["CH Gayle","V Kohli","AB de Villiers","SR Watson","SN Khan","KM Jadhav","STR Binny","Parvez Rasool","HV Patel","AF Milne","YS Chahal"] 
# team1bowlers=["AF Milne","SR Watson","Parvez Rasool","HV Patel","YS Chahal"]
# #191/5 rcb
# team2batsmen=["Q de Kock","SS Iyer","SV Samson","KK Nair","SW Billings","CH Morris","CR Braithwaite","RR Pant","Mohammed Shami","A Mishra","Z Khan"] 
# team2bowlers=["Z Khan","Mohammed Shami","CH Morris","CR Braithwaite","A Mishra"]
# #192/3 dd

# team1batsmen=["Q de Kock","SS Iyer","SV Samson","KK Nair","SW Billings","CH Morris","CR Braithwaite","RR Pant","Mohammed Shami","A Mishra","Z Khan"] 
# team1bowlers=["Z Khan","Mohammed Shami","CH Morris","CR Braithwaite","A Mishra"]
# # delhidaredevils 186/8 2016
# team2batsmen=["RV Uthappa","G Gambhir","PP Chawla","YK Pathan","SA Yadav","R Sathish","AD Russell","JO Holder","UT Yadav","SP Narine","GB Hogg"]
# team2bowlers=["AD Russell","JO Holder","SP Narine","UT Yadav","GB Hogg"]
# # kkr 159/allout


# team1batsmen=["M Vijay","MP Stoinis","SE Marsh","GJ Maxwell","Gurkeerat Singh","DA Miller","WP Saha","AR Patel","MM Sharma","KC Cariappa","Sandeep Sharma"] 
# team1bowlers=["Sandeep Sharma","MM Sharma","MP Stoinis","AR Patel","KC Cariappa"]
# # KXIP 154 2016 28th match
# team2batsmen=["DR Smith","BB McCullum","SK Raina","KD Karthik","RA Jadeja","DJ Bravo","Ishan Kishan","JP Faulkner","P Kumar","DS Kulkarni","S Kaushik"]
# team2bowlers=["P Kumar","DS Kulkarni","RA Jadeja","S Kaushik","DJ Bravo"]
# # GL 131/9


# team1batsmen=["V Kohli","KL Rahul","AB de Villiers","SR Watson","SN Khan","Mandeep Singh","STR Binny","HV Patel","KW Richardson","T Shami","Iqbal Abdulla"] 
# team1bowlers=["KW Richardson","HV Patel","SR Watson","T Shamsi","Iqbal Abdulla"]
# # rcb 185/3 2016 16th match
# team2batsmen=["AM Rahane","F du Plessis","KP Pietersen","SPD Smith","MS Dhoni","NLTC Perera","R Bhatia","R Ashwin","Ankit Sharma","M Ashwin","I Sharma"]
# team2bowlers=["I Sharma","NLTC Perera","Ankit Sharma","R Bhatia","R Ashwin","M Ashwin"]
# # RPS 172/8


# team1batsmen=["AM Rahane","F du Plessis","KP Pietersen","NLTC Perera","SPD Smith","MS Dhoni","IK Pathan","R Ashwin","I Sharma","Ankit Sharma","M Ashwin",]
# team1bowlers=["I Sharma","Ankit Sharma","R Ashwin","M Ashwin","NLTC Perera"]
# # RPS 152/7
# team2batsmen=["M Vijay","M Vohra","SE Marsh","DA Miller","GJ Maxwell","WP Saha","AR Patel","MM Sharma ","KJ Abbott","P Sahu","Sandeep Sharma"]
# team2bowlers=["Sandeep Sharma","KJ Abbott","AR Patel","P Sahu","MM Sharma"]
# # kingsXI 153/4


# team1batsmen=["DA Warner","S Dhawan","MC Henriques","EJG Morgan","DJ Hooda","NV Ojha","A Ashish Reddy","KV Sharma","B Kumar","Mustafizur Rahman","BB Sran"] 
# team1bowlers=["Z Khan","Mohammed Shami","CH Morris","CR Braithwaite","A Mishra"]
# # srh 142/7 2016
# team2batsmen=["RV Uthappa","G Gambhir","AD Russell","PP Chawla","YK Pathan","SA Yadav","R Sathish","AD Russell","JO Holder","UT Yadav","SP Narine","GB Hogg"]
# team2bowlers=["M Morkel","UT Yadav","Shakib Al Hasan","AD Russell","SP Narine"]
# # kkr 146/2  8th match


# team1batsmen=["CH Gayle","V Kohli","AB de Villiers","SR Watson","SN Khan","KM Jadhav","STR Binny","Parvez Rasool","HV Patel","AF Milne","YS Chahal"] 
# team1bowlers=["AF Milne","SR Watson","Parvez Rasool","HV Patel","YS Chahal"]
# #191/5 rcb
# team2batsmen=["Q de Kock","SS Iyer","SV Samson","KK Nair","SW Billings","CH Morris","CR Braithwaite","RR Pant","Mohammed Shami","A Mishra","Z Khan"] 
# team2bowlers=["Z Khan","Mohammed Shami","CH Morris","CR Braithwaite","A Mishra"]
# #192/3 dd


# team1batsmen=["CH Gayle","V Kohli","AB de Villiers","SR Watson","SN Khan","KM Jadhav","STR Binny","Parvez Rasool","HV Patel","AF Milne","YS Chahal"] 
# team1bowlers=["AF Milne","SR Watson","Parvez Rasool","HV Patel","YS Chahal"]
# #180/2 rcb
# team2batsmen=["DR Smith","BB McCullum","SK Raina","KD Karthik","RA Jadeja","DJ Bravo","Ishan Kishan","JP Faulkner","P Kumar","DS Kulkarni","S Kaushik"]
# team2bowlers=["P Kumar","DS Kulkarni","RA Jadeja","S Kaushik","DJ Bravo"]
# # GL 182-4


team1batsmen=["V Kohli","KL Rahul","AB de Villiers","SR Watson","SN Khan","Mandeep Singh","STR Binny","HV Patel","KW Richardson","T Shami","Iqbal Abdulla"] 
team1bowlers=["KW Richardson","HV Patel","SR Watson","T Shamsi","Iqbal Abdulla"]
# rcb 175/6
team2batsmen=["M Vijay","M Vohra","SE Marsh","DA Miller","GJ Maxwell","WP Saha","AR Patel","MM Sharma ","KJ Abbott","P Sahu","Sandeep Sharma"]
team2bowlers=["Sandeep Sharma","KJ Abbott","AR Patel","P Sahu","MM Sharma"]
# kingsXI 174/4

# team1batsmen=["PP Shaw","C Munro","SS Iyer","RR Pant","GJ Maxwell","V Shankar","LE Plunkett","A Mishra","S Nadeem","Avesh Khan","TA Boult"]
# team1bowlers=["S Nadeem","TA Boult","Avesh Khan","LE Plunkett","A Mishra"]
# #196-6
# team2batsmen=["DJM Short","JC Butler","SV Samson","BA Stokes","RA Tripathi","K Gowtham","JC Archer","AM Rahane","JD Unadkat","DS Kulkarni","S Gopal"]
# team2bowlers=["DS Kulkarni","JC Archer","K Gowtham","JD Unadkat","BA Stokes"]
# #146/5
# #2018 match 


# batsman1 , batsman2 , bowler_cluster , sr1 , avg1 , sr2 , avg2 , bowling_sr , bowling_avg , bowling_eco , bow_wkts
strike=team1batsmen[:2]
wickets=0
notout=[1,1]

while(overs!=20 and wickets<10):

    c_b_1, c_b_2, c_bow = access_bat_bowl(strike[0],strike[1],team2bowlers[(overs)%5])
    sr1 , avg1 = access_bat_stats(strike[0])
    sr2 , avg2 = access_bat_stats(strike[1])
    sr_bow , avg_bow , wkts = access_bow_stats(team2bowlers[(overs)%5])
    runs = model1.predict(array([c_b_1, c_b_2, c_bow,sr1, avg1 , sr2, avg2, sr_bow, avg_bow , wkts])) # returns prediction of runs from the model
    wicket = model2.predict(array([c_b_1, c_b_2, c_bow,sr1, avg1 , sr2, avg2, sr_bow, avg_bow , wkts])) #returns prediction pf wickets from the model

    runs = int(runs)
    wicket = float(wicket)
    #print(runs)
    #print('\n')
    #print(wicket)
    #print('\n')
    oldwickets=wickets
    wickets = wickets + wicket
    
    if(math.floor(wickets)-math.floor(oldwickets) == 1): #subtract the old total_wickets from the new value and with the difference the batsmen are changed accordingly
        if(math.floor(wickets)!=10):
            # strike[0] = team1batsmen[wickets+1]
            # wick_function1(math.floor(wickets))
            strike[0] = team1batsmen[int(math.floor(wickets))]
            strike[1] = team1batsmen[int(math.floor(wickets + 1))]


    score1 = score1 + runs
    # score1 = score1 + ((6-wicket)/6)*runs
    strike.reverse()
    print score1, "-" , int(wickets) ,"(",overs ,")\t+" , runs ,"|", strike[0],"|",strike[1],"\t|",team2bowlers[(overs)%5]
    overs = overs + 1

print "\n......................................\n"
		

strike=team2batsmen[:2]
overs=0
wickets=0

# second team to play
while(overs!=20 and wickets<10 and score1 >= score2):
    
    c_b_1, c_b_2, c_bow = access_bat_bowl(strike[0],strike[1],team1bowlers[(overs)%5])
    sr1 , avg1 = access_bat_stats(strike[0])
    sr2 , avg2 = access_bat_stats(strike[1])
    sr_bow , avg_bow , wkts = access_bow_stats(team1bowlers[(overs)%5])
    runs = model1.predict(array([c_b_1, c_b_2, c_bow,sr1, avg1 , sr2, avg2, sr_bow, avg_bow , wkts]))
    wicket = model2.predict(array([c_b_1, c_b_2, c_bow,sr1, avg1 , sr2, avg2, sr_bow, avg_bow , wkts]))
    
    runs = int(runs)
    wicket = float(wicket)
    
    oldwickets=wickets
    wickets = wickets + wicket
    
    if(math.floor(wickets)-math.floor(oldwickets) == 1):
        if(math.floor(wickets)!=10):
            # strike[0] = team1batsmen[wickets+1]
            # wick_function1(math.floor(wickets))
            strike[0] = team2batsmen[int(math.floor(wickets))]
            strike[1] = team2batsmen[int(math.floor(wickets + 1))]

    # score2 = score2 + ((6-wicket)/6)*runs
    score2 = score2 + runs
    strike.reverse()
    print score2, "-" , int(wickets) ,"(",overs ,")\t+" , runs ,"|", strike[0],"|",strike[1],"\t|",team1bowlers[(overs)%5]
    overs = overs + 1
    if(wickets == 10 or score2>score1):
        break


# if score1 == score2 its a draw
if score1 == score2:
    print "score of team1 is",score1
    print "score of team2 is", score2
    print " Its a draawww!! balle balle!!"
if score2>score1:
	print "score of team1 is ",score1
	print "score of team2 is ",score2
	print "Team 2 won with ",20-overs,"overs to spare!\n"
else:
	print "score of team1 is ",score1
	print "score of team2 is ",score2
	print "Team 1 won with ",score1-score2,"runs\n"

